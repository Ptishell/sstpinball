LAMP_ROW    EQU $2008
LAMP_AUX    EQU $2009
LAMP_COL    EQU $200A
BANK_SELECT EQU $3200

wdr         .MACRO
            clra
            sta BANK_SELECT
            .ENDM

delay       .MACRO i
            lda i
@l:         deca
            bne @l
            .ENDM

            .ORG 0xC000
main:
            ldx #lamps
            clrb
            stb LAMP_AUX
            incb

loop:
            clra
            sta LAMP_ROW
            sta LAMP_COL
            delay #$1F
            lda ,x+
            sta LAMP_COL
            stb LAMP_ROW
            delay #$1F
            wdr
            lslb
            bne loop
            bcc main
            rolb
            stb LAMP_AUX
            clrb
            bra loop

default_handler:
            rti

lamps:
            DB $01, $00, $00, $00, $00
            DB $00, $1C, $B6, $9F, $00

            .ORG 0xFFF0
reserved:   DW default_handler
            .ORG 0xFFF2
swi3:       DW default_handler
            .ORG 0xFFF4
swi2:       DW default_handler
            .ORG 0xFFF6
firq:       DW default_handler
            .ORG 0xFFF8
irq:        DW default_handler
            .ORG 0xFFFA
swi:        DW default_handler
            .ORG 0xFFFC
nmi:        DW default_handler
            .ORG 0xFFFE
reset:      DW main
