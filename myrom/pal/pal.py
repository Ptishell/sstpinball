#! /usr/bin/python

import electruth.truthtable as tt

def fill_entry(f, entry, bit, value):
    l = [entry[0] & 1,
        (entry[0] >> 1) & 1,
        (entry[0] >> 2) & 1,
        (entry[0] >> 3) & 1,
        (entry[0] >> 4) & 1,
        (entry[0] >> 5) & 1,
        (entry[0] >> 6) & 1,
        (entry[0] >> 7) & 1,
        (entry[0] >> 8) & 1,
        (entry[0] >> 9) & 1,
        (entry[0] >> 10) & 1,
        (entry[0] >> 11) & 1,
        (entry[0] >> 12) & 1,
        int((entry[1] >> bit) & 1 == value)
        ]
    f.write(",".join([str(i) for i in l]) + "\n")

def write_header(f):
    f.write("<A15,<A14,<A13,<NE,<Q,<AVMA,<RW,<A11,<A12,<MPIN,<A9,<A10,<XA0,>out\n")

def convert_tt(path):
    f = open(path, "r")
    out = open("out.csv", "w+")
    write_header(out)
    for line in f:
        entry = [int(x, 16) for x in line.split(" ")]
        fill_entry(out, entry, 7, 0)
    f.close()
    out.close()

def main():
    convert_tt("truthtable")
    truth_tables = tt.parse_raw_truthtable("out.csv", ',', False)
    truth_table = truth_tables['out']
    print(truth_table.shorten())

if __name__ == "__main__":
    main()
