DEV ?= /dev/ttyUSB0
BINDIFF = vbindiff

ROMCPU = rom/cpu/2.01
ROMDISP = rom/display/2.00

AS = tpasm

CPUSIZE = 512
DISPSIZE = 512

SMSDEV = sms/smsdev.py

TMPROM = /tmp/sst.rom

all::

myrom::
	$(MAKE) -C $@

flashcpu::
	cat $(ROMCPU) $(ROMCPU) $(ROMPU) $(ROMCPU) > $(TMPROM)
	./$(SMSDEV) -d $(DEV) -i $(TMPROM) -s $(CPUSIZE) write

flashdisp::
	./$(SMSDEV) -d $(DEV) -i $(ROMDISP) -s $(DISPSIZE) write

clean::
	$(MAKE) -C $@ clean
