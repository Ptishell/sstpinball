# Sega Starship Trooper Pinball Hack

## 27C040 - 27C010 Pinout

                ----_----
          Vpp -|   \_/   |- Vcc
          A16 -|         |- A18
          A15 -|         |- A17
          A12 -|         |- A14
          A07 -|         |- A13
          A06 -|         |- A08
          A05 -|         |- A09
          A04 -|         |- A11
          A03 -|         |- A15
          A02 -|         |- A10
          A01 -|         |- ~CE
          A00 -|         |- D07
          D00 -|         |- D06
          D01 -|         |- D05
          D02 -|         |- D04
          GND -|         |- D03
                ---------

## Homemade flash memory programmer

![Homemade flash memory programmer](http://www.psurply.com/sms/sms07.jpg)

### A29040C to Arduino Mega 1280

                ----_----
    19  - A18 -|   \_/   |- VCC - 5V
    21  - A16 -|         |- ~WR - 51
    42  - A15 -|         |- A17 - 20
    45  - A12 -|         |- A14 - 43
    30  - A07 -|         |- A13 - 44
    31  - A06 -|         |- A08 - 49
    32  - A05 -| A29040C |- A09 - 48
    33  - A04 -|         |- A11 - 46
    34  - A03 -|         |- ~OE - 52
    35  - A02 -|         |- A10 - 47
    36  - A01 -|         |- ~CE - 53
    37  - A00 -|         |- D07 - 29
    22  - D00 -|         |- D06 - 28
    23  - D01 -|         |- D05 - 27
    24  - D02 -|         |- D04 - 26
    GND - GND -|         |- D03 - 25
                ---------

## U213 - PAL16L8 Configuration

    ~ROMCS = A15 || A14
    ~RAMCS = !A15 && !A14 && !A13 && (!A12 || !A11 || !A10 || !A9 || RW || MPIN)
    IOPORT = !(!A15 && !A14 && A13 && !A12 && !A11 && !XA0)
    IOSTB = !A15 && !A14 && A13 && !A11

## Memory map

- `0000`-`1FFF` : RAM
    - `0000`-`1DFF` : Read/Write Area
    - `1E00`-`1FFF` : Write Protected Area
- `2000`-`27FF` : IOPORT
    - `2000` : HIGH CURRENT SOLENOIDS A
        - bit 0 : Left Turbo Bumper
        - bit 1 : Bottom Turbo Bumper
        - bit 2 : Right Turbo Bumper
        - bit 3 : Left Slingshot
        - bit 4 : Right Singshot
        - bit 5 : Mini Flipper
        - bit 6 : Left Flipper
        - bit 7 : Right Flipper
    - `2001` : HIGH CURRENT SOLENOIDS B
        - bit 0 : Trough Up-Kicker
        - bit 1 : Auto Launch
        - bit 2 : Vertical Up-Kicker
        - bit 3 : Super Vertical Up-Kicker
        - bit 4 : Left Magnet
        - bit 5 : Right Magnet
        - bit 6 : Brain Bug
        - bit 7 : European Token Dipenser (not used)
    - `2002` : LOW CURRENT SOLENOIDS
        - bit 0 : Stepper Motor #1
        - bit 1 : Stepper Motor #2
        - bit 2 : Stepper Motor #3
        - bit 3 : Stepper Motor #4
        - bit 4 : *not used*
        - bit 5 : *not used*
        - bit 6 : Flash Brain Bug
        - bit 7 : Option Coin Meter
    - `2003` : FLASH LAMPS DRIVERS
        - bit 0 : Flash Red
        - bit 1 : Flash Yellow
        - bit 2 : Flash Green
        - bit 3 : Flash Blue
        - bit 4 : Flash Multiball
        - bit 5 : Flash Lt. Ramp
        - bit 6 : Flash Rt. Ramp
        - bit 7 : Flash Pops
    - `2004` : *N/A*
    - `2005` : *N/A*
    - `2006` : AUX. OUT PORT (not used)
    - `2007` : AUX. IN PORT (not used)
    - `2008` : LAMP RETURNS
    - `2009` : AUX. LAMPS
    - `200A` : LAMP DRIVERS
- `3000`-`37FF` : IO
    - `3000` : DEDICATED SWITCH IN
        - bit 0 : Left Flipper Button
        - bit 1 : Left Flipper End-of-Stroke
        - bit 2 : Right Flipper Button
        - bit 3 : Right Flipper End-of-Stroke
        - bit 4 : Mini Flipper Button
        - bit 5 : Red Button
        - bit 6 : Green Button
        - bit 7 : Black Button
    - `3100` : DIP SWITCH
    - `3200` : BANK SELECT
    - `3300` : SWITCH MATRIX COLUMNS
    - `3400` : SWITCH MATRIX ROWS
    - `3500` : PLASMA IN
    - `3600` : PLASMA OUT
    - `3700` : PLASMA STATUS
- `4000`-`7FFF` : ROM
- `8000`-`BFFF` : ROM (Mirror)
- `C000`-`FFFF` : ROM (Mirror)
